<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Aws static website using s3 and cloudfront with aws dns

## Usage

```hcl-terraform
provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

module "aws-s3-static-website-cloudfront-with-aws-dns" {
  source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-s3-static-website-cloudfront-with-aws-dns?ref=v1.0.0"

  # optional, default = ""
  build                     = "<Path to your built application>"

  hosted_zone_id            = "AWS Hosted zone id"
  domain_name               = "<Custom domain name>"
  subject_alternative_names = ["Some alternative name"]
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13.0 |
| aws | >= 2.0 |
| null | >= 2.0 |
| template | 2.2.0 |

## Providers

| Name | Version |
|------|---------|
| aws | 4.38.0 |
| aws.us-east-1 | 4.38.0 |
| null | 3.2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| aws\_domain\_cert\_acm\_with\_aws\_dns | git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-domain-cert-acm-with-aws-dns | v1.0.3 |
| waf | cloudposse/waf/aws | v0.0.4 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | resource |
| [aws_cloudfront_origin_access_identity.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_access_identity) | resource |
| [aws_cloudfront_response_headers_policy.cloudfront_distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_response_headers_policy) | resource |
| [aws_kms_key.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_route53_record.aws_acm_validation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_s3_bucket.cloudfront_logging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.cloudfront_logging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_acl.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_cors_configuration.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration) | resource |
| [aws_s3_bucket_logging.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging) | resource |
| [aws_s3_bucket_policy.allow_access_from_another_account](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.cloudfront_logging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.website](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.cloudfront_logging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_versioning.cloudfront_logging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_versioning.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_website_configuration.s3_front_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | resource |
| [null_resource.cloudfront_invalidation](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.s3_sync](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [aws_iam_policy_document.s3_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| build | (Optional) The path to the folder where you built your website (absolute or relative) | `string` | `""` | no |
| cloudfront\_allowed\_methods | (Optional) Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin | `list(string)` | <pre>[<br>  "GET",<br>  "HEAD"<br>]</pre> | no |
| cloudfront\_cached\_methods | (Optional) Controls whether CloudFront caches the response to requests using the specified HTTP methods | `list(string)` | <pre>[<br>  "GET",<br>  "HEAD"<br>]</pre> | no |
| comment | (Optional) Comment for the origin access identity | `string` | `"Managed by Terraform"` | no |
| content\_security\_policy | (Optional) The policy directives and their values that CloudFront includes as values for the Content-Security-Policy HTTP response header. | `string` | `"frame-ancestors 'none'; default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"` | no |
| default\_root\_object | (Optional) Object that CloudFront return when requests the root URL | `string` | `"index.html"` | no |
| default\_ttl | (Optional) The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header | `number` | `3600` | no |
| distribution\_enabled | (Optional) Whether the distribution is enabled to accept end user requests for content | `bool` | `true` | no |
| dns\_ttl | (Optional) Ttl of the dns records | `number` | `3600` | no |
| domain\_name | Domain name | `string` | n/a | yes |
| error\_document | (Optional) An absolute path to the document to return in case of a 4XX error | `string` | `"index.html"` | no |
| forward\_cookies | (Optional) Specifies whether you want CloudFront to forward all or no cookies to the origin. Can be 'all' or 'none' | `string` | `"none"` | no |
| geo\_restriction\_locations | (Optional) List of country codes for which  CloudFront either to distribute content (whitelist) or not distribute your content (blacklist) | `list(string)` | `[]` | no |
| geo\_restriction\_type | (Optional) Method that use to restrict distribution of your content by country: `none`, `whitelist`, or `blacklist` | `string` | `"none"` | no |
| hosted\_zone\_id | AWS hosted zone ID | `string` | n/a | yes |
| index\_document | (Optional) Amazon S3 returns this index document when requests are made to the root domain or any of the subfolders | `string` | `"index.html"` | no |
| ipv6\_enabled | (Optional) Whether the IPv6 is enabled for the distribution | `bool` | `true` | no |
| max\_ttl | (Optional) The maximum amount of time that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated | `number` | `86400` | no |
| min\_ttl | (Optional) The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated | `number` | `0` | no |
| minimum\_protocol\_version | (Optional) Cloudfront TLS minimum protocol version. | `string` | `"TLSv1.2_2021"` | no |
| ssl\_support\_method | (Optional) Specifies how you want CloudFront to serve HTTPS requests | `string` | `"sni-only"` | no |
| subject\_alternative\_names | Alternative names | `list(string)` | n/a | yes |
| tettt | n/a | `string` | `""` | no |
| viewer\_protocol\_policy | (Optional) Use this element to specify the protocol that users can use to access the files in the origin specified by TargetOriginId when a request matches the path pattern in PathPattern. One of 'allow-all', 'https-only', or 'redirect-to-https' | `string` | `"redirect-to-https"` | no |

## Outputs

| Name | Description |
|------|-------------|
| distribution\_id | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contribution
- [Contributing](./CONTRIBUTING.md)
