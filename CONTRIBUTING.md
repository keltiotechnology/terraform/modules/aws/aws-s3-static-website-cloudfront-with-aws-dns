# Developper guide

## Prerequisites
- [pre-commit](https://pre-commit.com/)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)
- [checkov](https://github.com/bridgecrewio/checkov)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [terraform-config-inspect](https://github.com/hashicorp/terraform-config-inspect)

## Setup pre-commit
```bash
pre-commit install
```
