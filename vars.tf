/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Common Variables
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "domain_name" {
  type        = string
  description = "Domain name"
}

variable "hosted_zone_id" {
  type        = string
  description = "AWS hosted zone ID"
}

variable "build" {
  type        = string
  description = "(Optional) The path to the folder where you built your website (absolute or relative)"
  default     = ""
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for AWS CERTIFICATE
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "subject_alternative_names" {
  type        = list(string)
  description = "Alternative names"
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for DNS
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "dns_ttl" {
  type        = number
  description = "(Optional) Ttl of the dns records"
  default     = 3600
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for S3 Bucket
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "index_document" {
  type        = string
  description = "(Optional) Amazon S3 returns this index document when requests are made to the root domain or any of the subfolders"
  default     = "index.html"
}

variable "error_document" {
  type        = string
  description = "(Optional) An absolute path to the document to return in case of a 4XX error"
  default     = "index.html"
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for Cloudfront Distribution
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "distribution_enabled" {
  type        = bool
  description = "(Optional) Whether the distribution is enabled to accept end user requests for content"
  default     = true
}

variable "ipv6_enabled" {
  type        = bool
  description = "(Optional) Whether the IPv6 is enabled for the distribution"
  default     = true
}

variable "comment" {
  type        = string
  description = "(Optional) Comment for the origin access identity"
  default     = "Managed by Terraform"
}

variable "default_root_object" {
  type        = string
  description = "(Optional) Object that CloudFront return when requests the root URL"
  default     = "index.html"
}

variable "geo_restriction_type" {
  type = string

  description = "(Optional) Method that use to restrict distribution of your content by country: `none`, `whitelist`, or `blacklist`"
  # e.g. "whitelist"
  default = "none"
}

variable "geo_restriction_locations" {
  type = list(string)

  description = "(Optional) List of country codes for which  CloudFront either to distribute content (whitelist) or not distribute your content (blacklist)"
  # e.g. ["US", "CA", "GB", "DE"]
  default = []
}

variable "cloudfront_allowed_methods" {
  type        = list(string)
  description = "(Optional) Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin"
  default     = ["GET", "HEAD"]
}

variable "cloudfront_cached_methods" {
  type        = list(string)
  description = "(Optional) Controls whether CloudFront caches the response to requests using the specified HTTP methods"
  default     = ["GET", "HEAD"]
}

variable "forward_cookies" {
  type        = string
  description = "(Optional) Specifies whether you want CloudFront to forward all or no cookies to the origin. Can be 'all' or 'none'"
  default     = "none"
}

variable "viewer_protocol_policy" {
  type        = string
  description = "(Optional) Use this element to specify the protocol that users can use to access the files in the origin specified by TargetOriginId when a request matches the path pattern in PathPattern. One of 'allow-all', 'https-only', or 'redirect-to-https'"
  default     = "redirect-to-https"
}

variable "default_ttl" {
  type        = number
  description = "(Optional) The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header"
  default     = 3600
}

variable "min_ttl" {
  type        = number
  description = "(Optional) The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated"
  default     = 0
}

variable "max_ttl" {
  type        = number
  description = "(Optional) The maximum amount of time that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated"
  default     = 86400
}

variable "ssl_support_method" {
  type        = string
  description = "(Optional) Specifies how you want CloudFront to serve HTTPS requests"
  default     = "sni-only"
}

variable "minimum_protocol_version" {
  type        = string
  description = "(Optional) Cloudfront TLS minimum protocol version."
  default     = "TLSv1.2_2021"
}

variable "content_security_policy" {
  type        = string
  description = "(Optional) The policy directives and their values that CloudFront includes as values for the Content-Security-Policy HTTP response header."
  default     = "frame-ancestors 'none'; default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"
}
