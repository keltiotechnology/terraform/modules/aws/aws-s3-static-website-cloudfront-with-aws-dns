/*
 * # Aws static website using s3 and cloudfront with aws dns
 *
 * ## Usage
 * 
 * ```hcl-terraform
 * provider "aws" {
 *   region = "eu-west-1"
 * }
 * 
 * provider "aws" {
 *   alias  = "us-east-1"
 *   region = "us-east-1"
 * }
 * 
 * module "aws-s3-static-website-cloudfront-with-aws-dns" {
 *   source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-s3-static-website-cloudfront-with-aws-dns?ref=v1.0.0"
 *  
 *   build                     = "<Path to your built application>"
 * 
 *   hosted_zone_id            = "AWS Hosted zone id"
 *   domain_name               = "<Custom domain name>"
 *   subject_alternative_names = ["Some alternative name"]
 * }
 * ```
 *
*/
locals {
  website_records = concat([var.domain_name], var.subject_alternative_names)
}

module "aws_domain_cert_acm_with_aws_dns" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-domain-cert-acm-with-aws-dns?ref=v1.0.3"

  hosted_zone_id            = var.hosted_zone_id
  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names

  providers = {
    aws = aws.us-east-1
  }
}

resource "aws_kms_key" "s3" {
  description             = "KMS key for ${var.domain_name}"
  deletion_window_in_days = 10
  enable_key_rotation     = true
}

#tfsec:ignore:aws-s3-enable-bucket-logging Skipping `Ensure the S3 bucket has access logging enabled` because this is already the logging bucket.
resource "aws_s3_bucket" "cloudfront_logging" {
  #bridgecrew:skip=BC_AWS_GENERAL_72:Skipping `Ensure S3 bucket has cross-region replication enabled` because this is out of scope of this module's use case.
  #bridgecrew:skip=CKV_AWS_18:Skipping `Ensure the S3 bucket has access logging enabled` because this is already the logging bucket.
  bucket = "logging.${var.domain_name}"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "cloudfront_logging" {
  bucket = aws_s3_bucket.cloudfront_logging.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.s3.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_acl" "cloudfront_logging" {
  bucket = aws_s3_bucket.cloudfront_logging.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "cloudfront_logging" {
  bucket = aws_s3_bucket.cloudfront_logging.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "cloudfront_logging" {
  bucket = aws_s3_bucket.cloudfront_logging.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket" "s3_front_bucket" {
  #bridgecrew:skip=BC_AWS_GENERAL_72:Skipping `Ensure S3 bucket has cross-region replication enabled` because this is out of scope of this module's use case.
  #bridgecrew:skip=BC_AWS_GENERAL_56:Skipping `Ensure S3 buckets are encrypted with KMS by default` because this module has configurable encryption via `var.encryption_enabled`.
  bucket = var.domain_name
}

resource "aws_s3_bucket_logging" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.id

  target_bucket = aws_s3_bucket.cloudfront_logging.id
  target_prefix = "log/"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.s3.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.s3_front_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.cloudfront_distribution.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = aws_s3_bucket.s3_front_bucket.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_s3_bucket_website_configuration" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.bucket

  index_document {
    suffix = var.index_document
  }

  error_document {
    key = var.error_document
  }
}

resource "aws_s3_bucket_acl" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_cors_configuration" "s3_front_bucket" {
  bucket = aws_s3_bucket.s3_front_bucket.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT", "POST", "DELETE", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  cors_rule {
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
  }
}

# Explicitly allow S3 content to be publicly accessible
resource "aws_s3_bucket_public_access_block" "website" {
  bucket                  = aws_s3_bucket.s3_front_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

module "waf" {
  source  = "cloudposse/waf/aws"
  version = "v0.0.4"

  name           = var.domain_name
  scope          = "CLOUDFRONT"
  default_action = "allow"

  providers = {
    aws = aws.us-east-1
  }
}

resource "aws_cloudfront_response_headers_policy" "cloudfront_distribution" {
  name    = "${replace(var.domain_name, ".", "-")}-policy"
  comment = "CDN policy"

  cors_config {
    access_control_allow_credentials = false

    access_control_allow_headers {
      items = ["*"]
    }

    access_control_allow_methods {
      items = ["GET"]
    }

    access_control_allow_origins {
      items = ["*"]
    }

    origin_override = true
  }

  security_headers_config {
    content_type_options {
      override = true
    }
    frame_options {
      frame_option = "DENY"
      override     = true
    }
    referrer_policy {
      referrer_policy = "same-origin"
      override        = true
    }
    xss_protection {
      mode_block = true
      protection = true
      override   = true
    }
    strict_transport_security {
      access_control_max_age_sec = "63072000"
      include_subdomains         = true
      preload                    = true
      override                   = true
    }
    content_security_policy {
      content_security_policy = var.content_security_policy
      override                = true
    }
  }
}

resource "aws_cloudfront_origin_access_identity" "cloudfront_distribution" {
  comment = "Cloudfront origin access identity"
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  enabled             = var.distribution_enabled
  is_ipv6_enabled     = var.ipv6_enabled
  comment             = var.comment
  default_root_object = var.default_root_object

  origin {
    domain_name = aws_s3_bucket.s3_front_bucket.bucket_regional_domain_name
    origin_id   = var.domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cloudfront_distribution.cloudfront_access_identity_path
    }
  }

  aliases = local.website_records

  restrictions {
    geo_restriction {
      restriction_type = var.geo_restriction_type
      locations        = var.geo_restriction_locations

    }
  }

  default_cache_behavior {
    target_origin_id = var.domain_name

    allowed_methods = var.cloudfront_allowed_methods
    cached_methods  = var.cloudfront_cached_methods

    forwarded_values {
      query_string = false

      cookies {
        forward = var.forward_cookies
      }
    }

    viewer_protocol_policy     = var.viewer_protocol_policy
    response_headers_policy_id = aws_cloudfront_response_headers_policy.cloudfront_distribution.id
    default_ttl                = var.default_ttl
    min_ttl                    = var.min_ttl
    max_ttl                    = var.max_ttl
  }

  viewer_certificate {
    acm_certificate_arn      = module.aws_domain_cert_acm_with_aws_dns.aws_acm_certificate_arn
    ssl_support_method       = var.ssl_support_method
    minimum_protocol_version = var.minimum_protocol_version
  }

  logging_config {
    bucket = aws_s3_bucket.cloudfront_logging.bucket_domain_name
    prefix = "cloudfront"
  }

  web_acl_id = module.waf.arn

  provider = aws.us-east-1
}

resource "aws_route53_record" "aws_acm_validation" {
  for_each = {
    for dvo in local.website_records : dvo => {
      name = dvo
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = ["${aws_cloudfront_distribution.cloudfront_distribution.domain_name}."]
  ttl             = var.dns_ttl
  type            = "CNAME"
  zone_id         = var.hosted_zone_id
}

# Uploads the files to the s3 Bucket
resource "null_resource" "s3_sync" {
  count = var.build != "" ? 1 : 0

  depends_on = [
    aws_s3_bucket.s3_front_bucket
  ]
  provisioner "local-exec" {
    command = "aws s3 sync ${var.build} s3://${var.domain_name}"
  }
}

# Creates the invalidation for the Cloudfront distribution (Needs AWS CLI to be setup)
resource "null_resource" "cloudfront_invalidation" {
  count = var.build != "" ? 1 : 0

  depends_on = [
    aws_cloudfront_distribution.cloudfront_distribution
  ]
  provisioner "local-exec" {
    command = "aws cloudfront create-invalidation --distribution-id ${aws_cloudfront_distribution.cloudfront_distribution.id} --paths '/*'"
  }
}
