provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

module "aws-s3-static-website-cloudfront-with-aws-dns" {
  source = "../../"

  hosted_zone_id            = var.hosted_zone_id
  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names

  providers = {
    aws           = aws
    aws.us-east-1 = aws.us-east-1
  }
}
