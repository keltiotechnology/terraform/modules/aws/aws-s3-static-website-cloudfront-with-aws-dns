variable "hosted_zone_id" {
  type        = string
  description = "AWS hosted zone ID"
}

variable "domain_name" {
  type        = string
  description = "The website domain name"
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "Altername names to add in the website certificate"
}
